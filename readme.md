HALLO LIEVE LINUX BOYS HOE WERKT DEZE APPLICATIE:

Ten eerste wat regels:
    - Bij voorkeur gebruik VS Code als IDE
    - PAS NOOIT, MAAR DAN OOK NOOIT ANDERE FILES AAN DAN \express-swagger\Mr_Linux_project-API_Medi_Maatje-1.0.0-swagger.yaml

STAP 1 ZORG DAT JE NPM HEBT GEINSTALLEERD: https://www.npmjs.com/get-npm

STAP 2 CD IN DE ROOT MAP MET JE CLI ($/express-swagger)

STAP 3 DOE DEZE COMMAND: npm install

STAP 4 DOE DEZE COMMAND: npm install --save yamlj

STAP 5 DOE DEZE COMMAND: npm install --save swagger-ui-express

WE GAAN NU EERST TESTEN OF JOUW APPLICATIE GOED IS GECONFIGUREERD

STAP 6 DOE DEZE COMMAND: npm start

STAP 7 GA NAAR DE URL: http://localhost:3000/api-docs/

zie je nu de swagger-ui de specificatie? 10/10 je bent amazing je hebt het gedaan!
Je kan nu de yaml file aanpassen. Om je aanpassing in swagger-ui te tonen heb ik slecht nieuws...
Stop je runtime (ctrl + C) via de commandline en start opnieuw met npm start om de veranderingen te zien.

TENZIJ (HACKERMAND HEEFT DIT WEER UITGEVONDEN VOOR JULLIE) je nodemon installeert

npm install -g nodemon

en je daarna de applicatie runt met:

nodemon --ext yaml

LET OP! je moet na je change wel je browser refreshen lots of love.